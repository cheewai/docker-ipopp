#! /bin/bash
#
# Dockerfile entrypoint script run as root
# Requires container runtime conditions:
#   - mount /home 
#   - environment variables IPOPP_UID
#   - command/script to execute at the end
#

set -ux

[ $IPOPP_UID -gt 0 ] || {
  echo "Great security risk to use uid 0" >&2
  exit 1
}

# Create ipopp user
/usr/sbin/useradd -u $IPOPP_UID --shell '/bin/bash' --groups=wheel ipopp
[ -f /home/ipopp/.bash_profile ] || {
  echo 'source /home/ipopp/drl/standalone/bash_profile_tail' >>/etc/skel/.bash_profile
  echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ipopp/drl/standalone/jdk1.8.0_45/jre/lib/amd64/server' >>/etc/skel/.bash_profile
}
rsync -auv /etc/skel/ /home/ipopp/
chown -R $IPOPP_UID:$IPOPP_UID /home/ipopp
echo 'PS1="\[\e]0;\u@\h: \w\a\]\u@\h:\w$ "' >> /etc/profile

# If container is passed any argument, execute and exit
# Use sudo instead of su for better terminal/signal/argument handling
[ $# -gt 0 ] && exec sudo -i -u ipopp "$@"

rsyslogd -c5

if [ ! -d "/home/ipopp/drl" ] ; then
  su - ipopp -c 'cd /home/ipopp/IPOPP && ./install_ipopp.sh'
  #TODO: how about SPA update subsequent to IPOPP release
  # This is first-run, exit here so that user can edit drl/ncs/configs/default_config.file
  echo "***"
  echo "*** IPOPP installed, please edit /home/ipopp/drl/ncs/configs/default_config.file"
  echo "***"
  exit 0
fi

#
# Normal run after IPOPP has been installed and configured
#
function shut_down() {
  su - ipopp -c 'echo "Caught signal" >&2; mysql_check -stop; /home/ipopp/drl/tools/services.sh stop'
  exit
}

su - ipopp -c "/home/ipopp/drl/tools/services.sh start"
trap "shut_down" SIGKILL SIGTERM SIGHUP SIGINT EXIT
sleep 5s
su - ipopp -c "/home/ipopp/drl/tools/services.sh status"

# this combination is needed to trap signals from container terminating
tail -f /home/ipopp/drl/is/logs/*.log /home/ipopp/drl/dsm/jsw/logs/*.log /home/ipopp/drl/nsls/jsw/logs/*.log
wait
