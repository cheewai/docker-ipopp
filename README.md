# IPOPP in a docker

This repo creates a docker image containing the baseline operating system along with packages required to run the NASA [Direct Readout Laboratory](directreadout.sci.gsfc.nasa.gov/)(DRL) [IPOPP software](http://directreadout.sci.gsfc.nasa.gov/?id=dspContent&cid=68)

CentOS 7.5 has been tested successfully with IPOPP 2.6 which includes Java runtime and MySQL server. You may use this repo as a guide but switch to use CentOS 7+ as base image if you can use IPOPP 2.6 or later.


## WARNING

VIIRS C-SDR version 2.4 and higher requires CentOS 7. This repo uses CentOS 6 as base image so it cannot be used.


## Download IPOPP

It does not include the IPOPP software itself which you have to download from the DRL web site. Basically, you should download the latest IPOPP tar.gz as well as any individual SPA which are released subsequent to IPOPP's release date.

## Unpack IPOPP tarball

You need to set aside a directory with adequate free space (150 GB minimum for software installation and processing, excluding data archiving) that will be mounted as */home/ipopp/* in the Docker container. The sample *docker-compose.yml* given below assumes the directory to be *./npp_home/* located in the same directory as *docker-compose.yml*.

```
tar -C npp_home -xvf DRL-IPOPP_2.5.tar.gz
```

This results in a directory tree being created in *npp_home/IPOPP* (*/home/ipopp/IPOPP* in the container)


## Runtime requirements

* Ensure the hardware (RAM, CPU, etc.) meets the IPOPP requirements

* Ensure adequate free disk space in *npp_home* 

* Set environment variable IPOPP_UID which will become the UID of ipopp user inside docker container.



## Usage

You can either build the Docker image locally or use the autobuild image on Docker hub. Description in this section uses [docker-compose](https://docs.docker.com/compose/reference/docker-compose/). Alternatively, you can use  [docker CLI](https://docs.docker.com/reference/commandline/cli/) not explained here.

### Create docker-compose.yml

Create *docker-compose.yml* based on the example and edit it as appropriate.

Even though, the IPOPP software contains the code to process both MODIS `*.PDS` and NPP `*.dat` but it may be configured to process **either one** and not both at the same time.

The sample *docker-compose.yml* illustrates how you can run two Docker containers using the same IPOPP Docker image.  The home directories for each instance must be separate, i.e.

- *npp_home*
- *modis_home*

If you need to process both, repeat the same step to unpack IPOPP tarball described earlier to create *modis_home/IPOPP/*. Otherwise, you should omit the `ipopp_modis` stanza completely from your *docker-compose.yml*.


```
version: '2'
services:
  ipopp_npp:
    image: cheewai/docker-ipopp
    environment:
      # Set value to the real user numeric ID
      IPOPP_UID: 1000
    volumes:
      - ./npp_home:/home/ipopp
  restart: on-failure:5
  ipopp_modis:
    image: cheewai/docker-ipopp
    environment:
      # Set value to the real user numeric ID
      IPOPP_UID: 1000
    volumes:
      - ./modis_home:/home/ipopp
  restart: on-failure:5
```

Another example of *docker-compose.yml* for processing NPP only where the Docker host is CentOS 7+, and allowing IPOPP Dashboard Java-based X graphical UI. The **DISPLAY** environment variable must match the actual value in the X console.

```
ipopp_npp:
  image: cheewai/docker-ipopp
  container_name: ipopp_npp
  environment:
    IPOPP_UID: 1000
    # Must match the actual value in X console
    DISPLAY: ':0'
  privileged: true
  volumes:
    - /tmp/.X11-unix:/tmp/.X11-unix
    - ./npp_home:/home/ipopp
  restart: on-failure:5
```

If your X11 server runs separately from the docker host, you need to `ssh -X docker_host`, instead of mouting `.X11-unix`, you will need the following. Read https://dzone.com/articles/docker-x11-client-via-ssh for detailed explanation.

```
  net: host
  volumes:
    - $HOME/.Xauthority:/home/ipopp/.Xauthority:rw
```

### First-run to install IPOPP software

Run the container for the first time to install the IPOPP software. This may take up to 30 minutes to complete.

```
docker-compose up
```

The Docker image has defined a Docker entrypoint script */start.sh* which gets executed automatically when a Docker container is instantiated. The script expects to find the complete set the software to be found in */home/ipopp/IPOPP/*.

*start.sh* will perform IPOPP installation into */home/ipopp/drl/* if the directory does not exist already. When the IPOPP installation script completes, *start.sh* will terminate the Docker container.

With *start.sh* as the Docker entrypoint script, if the image is run i.e. `docker-compose up`, `docker-compose run`, etc. without any argument, *start.sh* will start IPOPP. With arguments, *start.sh* will execute them as *ipopp* user in the container.

> Beware, if you override the entrypoint with another script, the *ipopp* user may not exist and cannot run any of the IPOPP script correctly due to missing environment variables.


### Configure IPOPP

After successul installation of IPOPP, you need to configure the IPOPP software. Outside the container, the path to the configuration file is `drl/ncs/configs/default_config.file`.

If you are having multiple instances of IPOPP, you will need to edit every configuration file.


### Run IPOPP operationally

The IPOPP software expects incoming data files to be saved to `drl/data/dsm/nisfes_data/` and it output to `drl/data/pub/gsfcdata/`

To run container in the background:

```
docker-compose up -d
```

### Start services

```
docker exec -u ipopp ipopp-npp bash -c 'source /home/ipopp/.profile && cd /home/ipopp/drl/tools && ./services.sh start'
```

### Launch graphical dashboard

```
docker exec -u ipopp ipopp-npp bash -c 'source /home/ipopp/.profile && cd /home/ipopp/drl/tools && ./dashboard.sh'
```

### Query status of services

```
docker exec -u ipopp ipopp-npp bash -c 'source /home/ipopp/.profile && cd /home/ipopp/drl/tools && ./services.sh status'
```

### Fix corrupted, excessive disk usage

When IPOPP is shut down abruptly instead of through `services.sh stop`, it may be left in a inconsistent or corrupted state and it will not start any more.

Over time, IPOPP may fail to remove unnecessary ancillary data and you run low i.e. less than 100 GB for per-overpass processing.

Solution:

```
docker exec -u ipopp ipopp-npp bash -c 'source /home/ipopp/.profile && cd /home/ipopp/drl/tools && ./reset_ipopp.sh'
```


## SPA update

* Save all downloaded SPA tar.gz into the actual subdirectory which corresponds to /home/ipopp/drl/ inside the container

* Shell into the running container named *ipopp_npp* for example. Repeat for the *ipopp_modis* container.

```
# if the container is running
docker-compose exec --user ipopp ipopp_npp bash -l

# if the container is not running
docker-compose run --rm ipopp_npp /bin/bash -l
```

* As user `ipopp` inside the container, run the following for every SPA update:

```
# For example the SPA is XYZ.tar.gz

source /home/ipopp/.profile
cd /home/ipopp/drl
/home/ipopp/drl/tools/install_spa.sh XYZ.tar.gz
/home/ipopp/drl/SPA/XYZ/NISGSinstall.sh

# Read SPA release guide; some SPA needs additional configuration
```
